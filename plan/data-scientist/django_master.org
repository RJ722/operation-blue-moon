#+TITLE: Plan
#+AUTHOR: Kurian Benoy
#+EMAIL: kurian.bkk@gmail.com
#+TAGS: READ WRITE DEV TASK EVENT MEETING

* GOALS
** Write five kaggle kernels

** Practise 250 algorithimc problems
** Learn the courses
*** Cutting edge Deep learning FastAI part2
***  CS231 course
***  ml.course.ai
** Help with DVC(my current open source org) and CloudCV
** Speak in one national/international conference
** Become a google code-in mentor

* PLAN
** July      16, 2019 - August     1, 2019 (17 days)
** August     2, 2019 - August    14, 2019 (13 days)
** August    15, 2019 - August    30, 2019 (16 days)
** August    31, 2019 - September 12, 2019 (13 days)
** September 13, 2019 - September 29, 2019 (17 days)
** Spetember 30, 2019 - October   12, 2019 (13 days)
** October   13, 2019 - October   28, 2019 (16 days)
** October   29, 2019 - November  11, 2019 (14 days)
** November  12, 2019 - November  27, 2019 (16 days)
** November  28, 2019 - December  11, 2019 (14 days)
** December  12, 2019 - December  26, 2019 (15 days)
** December  27, 2019 - January    9, 2020 (14 days)
** January   10, 2020 - January   25, 2020 (16 days)
** January   26, 2020 - February   8, 2020 (14 days)
** February   9, 2020 - February  24, 2020 (16 days)
** February  25, 2020 - March      8, 2020 (13 days)




